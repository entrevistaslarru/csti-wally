# Base image
FROM node:16-alpine

#optimza aplicacion indicando que se encuentra en ambiente de produccion

ENV IS_PROD=true

#usuario de aplicacion
USER node

# Create app directory
WORKDIR /home/node

# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY --chown=node:node ./package*.json ./
# Bundle app source
COPY --chown=node:node ./node_modules/ ./node_modules
# Bundle app source
COPY --chown=node:node  ./dist/ ./dist


# Start the server using the production build
CMD [ "node", "build/main.js" ]
#docker run -dit -p 3000:3000 admin
#npm i node-fetch
#npm i github:jlarru93/delivery-models