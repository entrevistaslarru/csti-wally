import { HttpStatus } from "@nestjs/common";
import { Builder } from "builder-pattern"
import { MetaData,Message, TypeError } from "../models"
import { DeliveryException } from "./delivery.exception";

export const MESSAGE_SUCCESS="satisfactorio"
export const INTERNAL_SERVER_ERROR="error interno en el servidor"
export const MESSAGE_NO_CONTENT="no se encontro información"
export const MESSAGE_WARN="advertencia, no se puede procesar la información"
export const USER_EXISTS="usuario ya registrado"
export const BRAND_EXISTS="marca ya registrada"
export const STORE_EXISTS="tienda ya registrada"
export const DELIVER_YMAN_EXISTS="motorizado ya registrado"
export const NOTAUTH="servicio no autorizado"

export const metaDataSucees=(idTransaccion:string,totalRecords?:number,totalNumberPages?:number)=>{
    return Builder<MetaData>()
        .idTransaction(idTransaccion)
        .totalRecords(totalRecords)
        .totalNumberPages(totalNumberPages)
        .messages([
            Builder<Message>()
            .code(HttpStatus.OK)
            .type(TypeError.info)
            .message(MESSAGE_SUCCESS)
            .build()
        ])
        .build();
}
export const metaDataSuceesCreate=(idTransaccion:string,totalRecords?:number,totalNumberPages?:number)=>{
    return Builder<MetaData>()
        .idTransaction(idTransaccion)
        .totalRecords(totalRecords)
        .totalNumberPages(totalNumberPages)
        .messages([
            Builder<Message>()
            .code(HttpStatus.CREATED)
            .type(TypeError.info)
            .message(MESSAGE_SUCCESS)
            .build()
        ])
        .build();
}

export const metaDataNoContent=(idTransaccion:string):DeliveryException=>{
    return new DeliveryException( HttpStatus.NO_CONTENT,Builder<MetaData>()
        .idTransaction(idTransaccion)
        .messages([
            Builder<Message>()
            .code(HttpStatus.NO_CONTENT)
            .type(TypeError.warn)
            .message(MESSAGE_NO_CONTENT)
            .build()
        ])
        .build());
}
export const metaDataWarn=(idTransaccion:string,message?:string,statusCode?:HttpStatus,customCode?:string):DeliveryException=>{
    return new DeliveryException( statusCode?statusCode:HttpStatus.BAD_REQUEST,Builder<MetaData>()
        .idTransaction(idTransaccion)
        .messages([
            Builder<Message>()
            .code(customCode?customCode:(statusCode?statusCode:HttpStatus.BAD_REQUEST))
            .type(TypeError.warn)
            .message(message?message:MESSAGE_WARN)
            .build()
        ])
        .build());
}

export const metaDataError=(idTransaccion:string,messague:string,code:string=null):DeliveryException=>{
    return new DeliveryException( HttpStatus.INTERNAL_SERVER_ERROR,Builder<MetaData>()
        .idTransaction(idTransaccion)
        .messages([
            Builder<Message>()
            .code(code?code:HttpStatus.INTERNAL_SERVER_ERROR)
            .type(TypeError.error)
            .message(messague?messague:INTERNAL_SERVER_ERROR)
            .build()
        ])
        .build());
}
export const metaDataValidateError=(idTransaccion:string,menssages:Message[])=>{
    return new DeliveryException( HttpStatus.BAD_REQUEST,Builder<MetaData>()
        .idTransaction(idTransaccion)
        .messages(menssages)
        .build());
}

export const defultListEmpty=(list:any[],defaultVal:any)=>{
    if(list==null || list.length===0){
        return list=[defaultVal];
    }
    return list;
}