import { HttpException, HttpStatus } from "@nestjs/common";
import { MetaData } from "../models";

export class DeliveryException extends HttpException {
    constructor(error:HttpStatus,public _meta:MetaData) {
        super(_meta, error)
    }
}