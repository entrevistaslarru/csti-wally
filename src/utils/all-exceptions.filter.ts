import { Catch, ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import { Builder } from 'builder-pattern';
import { Message, MetaData, TypeError } from '../models';
import { DeliveryException } from './delivery.exception';
@Catch()
export class AllExceptionsFilter extends BaseExceptionFilter {

  catch(exception: unknown, host: ArgumentsHost) {
    console.log("ingreso a control de errores generales")
    console.log(exception)
    const { httpAdapter } = this.httpAdapterHost;

    const ctx = host.switchToHttp();

    const httpStatus =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    /*const responseBody:MetaData = {
      statusCode: httpStatus,
      timestamp: "asdsadsd",
      path: httpAdapter.getRequestUrl(ctx.getRequest()),
    };*/
    
    if(exception instanceof DeliveryException){
      super.catch(exception,host)
    }else if(exception instanceof HttpException){
      console.log("error error no controlado")
      let responseBody =Builder<MetaData>()
        .idTransaction("")
        .messages([Builder<Message>().code("-1").message("error interno").type(TypeError.fatal).build()])
      .build();
      httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);
    }else{
      let responseBody =Builder<MetaData>()
        .idTransaction("")
        .messages([Builder<Message>().code("-1").message("error interno").type(TypeError.fatal).build()])
      .build();
      httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);
    }
  }
}