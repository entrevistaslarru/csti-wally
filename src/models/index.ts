import { HttpStatus } from "@nestjs/common";



export interface Message {
    code: HttpStatus | string
    type: TypeError
    message: string
}
export enum TypeError {
    error = "error", warn = "warn", invalid = "invalid", fatal = "fatal", info = "info"
}
export interface MetaData {
    messages?: Message[],
    totalRecords?: number
    idTransaction?: string
    nextPageNumber?: number
    totalNumberPages?: number
    additionalData?: string
    responseCode?: string
}
export interface ObjectResponse<T> {
    meta: MetaData
    data: T
}

export interface StoreFilterRequest {
    storeId?: number;
    word?: string;
    tag?: string[];
    categoryName?: string[];
    menuName?: string[];
    orderName?: string[];
    location: Location;
}

export class Location {
    lat: number;
    lng: number;
}

export interface S3Data {
    Location: string;
    ETag?: string;
    Bucket?: string;
    Key?: string;
}

export interface ProcedureResponse {
    RowDataPacket: RowDataPacket
    OkPacket: OkPacket
}
export interface RowDataPacket {
    value: number
}
export interface OkPacket {
    fieldCount?: number
    affectedRows?: number,
    insertId?: number,
    serverStatus?: number,
    warningCount?: number,
    message?: string,
    protocol41?: boolean,
    changedRows?: number
}