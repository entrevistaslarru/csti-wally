import { Injectable } from "@nestjs/common";

@Injectable()
export class ExchangeDb{
    

    exchanges:DbDataObject[]=[]
    constructor(){
        this.init()
    }

    private init(){
        this.exchanges.push({coin:"USD",value:1});
        this.exchanges.push({coin:"PE",value:0.26});
        this.exchanges.push({coin:"CO",value:0.00026});
        this.exchanges.push({coin:"ARG",value:0.00026});        
    }
    getAll() {
        return this.exchanges
    }
    add(exchange:DbDataObject){
        const currentExchange=this.exchanges.find(e=>e.coin===exchange.coin)
        if(currentExchange){
            throw Error("Moneda ya existente")
        }
        this.exchanges.push(exchange)
    }
    update(coin:string,exchange:DbDataObject){
        const indexUpdate=this.exchanges.findIndex(e=>e.coin==coin)
        if(indexUpdate<0){
            throw Error("No se encontro moneda")
        }
        this.exchanges[indexUpdate]=exchange
    }

    get(coin:string){
        return this.exchanges.find(e=>e.coin===coin)
    }
}


export class DbDataObject{
    coin:string
    value:number
}