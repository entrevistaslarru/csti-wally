import { CreateExchangeDto } from './create-exchange.dto';

export class UpdateExchangeDto extends CreateExchangeDto {}
