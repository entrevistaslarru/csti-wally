import { ApiProperty } from "@nestjs/swagger"

export class ExchangeRequest {
    @ApiProperty()
    value:number
    @ApiProperty()
    srcCoin:string
    @ApiProperty()
    dstCoint:string
}

export class ExchangeResponse {
    @ApiProperty()
    value:number
}
