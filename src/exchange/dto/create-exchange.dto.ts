import { ApiProperty } from "@nestjs/swagger"

export class CreateExchangeDto {
    @ApiProperty()
    coin:string
    @ApiProperty()
    value:number
}
