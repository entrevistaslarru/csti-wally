import { Injectable } from '@nestjs/common';
import { CreateExchangeDto } from './dto/create-exchange.dto';
import { UpdateExchangeDto } from './dto/update-exchange.dto';
import { ExchangeRequest, ExchangeResponse } from './dto/exchange.dto';
import { DbDataObject, ExchangeDb } from './db.exchange';
import { metaDataSucees, metaDataWarn } from '../utils';
import { v4 as uuidv4 } from 'uuid';
import { Builder } from 'builder-pattern';
import { ObjectResponse } from '../models';
import { JoiValidationPipe } from '../utils/joi-validation.pipe';
import { CreateExchangeValidation, ExchangeExchangeValidation, UpdateExchangeValidation } from './exchange.validate';
@Injectable()
export class ExchangeService {

  constructor(private readonly db: ExchangeDb) {

  }
  create(request: CreateExchangeDto) {
    const idTransaccion = uuidv4();
    new JoiValidationPipe(CreateExchangeValidation).transform(request, idTransaccion)
    this.db.add(request)
    return Builder<ObjectResponse<CreateExchangeDto>>()
      .meta(metaDataSucees(idTransaccion))
      .data(request)
      .build();
  }

  findAll() {
    const idTransaccion = uuidv4();
    const db=this.db.getAll()
    return Builder<ObjectResponse<DbDataObject[]>>()
      .meta(metaDataSucees(idTransaccion))
      .data(db)
      .build();
  }

  findOne(coin: string) {
    const idTransaccion = uuidv4();
    const db=this.db.get(coin)
    return Builder<ObjectResponse<DbDataObject>>()
      .meta(metaDataSucees(idTransaccion))
      .data(db)
      .build();
  }

  update(coin: string, requestBody: UpdateExchangeDto) {
    const idTransaccion = uuidv4();
    new JoiValidationPipe(UpdateExchangeValidation).transform({body:requestBody,coin:coin}, idTransaccion)
    this.db.update(coin,requestBody)
    return Builder<ObjectResponse<UpdateExchangeDto>>()
      .meta(metaDataSucees(idTransaccion))
      .data(requestBody)
      .build();
  }

  exchange(request: ExchangeRequest) {
    const idTransaccion = uuidv4();
    new JoiValidationPipe(ExchangeExchangeValidation).transform(request, idTransaccion)

    const srcCoinExchange = this.db.get(request.srcCoin)
    const dstCoinExchange = this.db.get(request.dstCoint)

    if (!srcCoinExchange) {
      throw metaDataWarn(idTransaccion, "moneda origen no disponible: " + request.srcCoin)
    }
    if (!dstCoinExchange) {
      throw metaDataWarn(idTransaccion, "moneda destino no disponible: " + request.dstCoint)
    }
    const usdValueOrigin = Number((request.value * srcCoinExchange.value).toFixed(2))
    const dstValue = Number((usdValueOrigin / dstCoinExchange.value).toFixed(2))

    return Builder<ObjectResponse<ExchangeResponse>>()
      .meta(metaDataSucees(idTransaccion))
      .data({ value: dstValue } as ExchangeResponse)
      .build();
  }
}
