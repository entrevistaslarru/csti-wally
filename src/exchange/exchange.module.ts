import { Module } from '@nestjs/common';
import { ExchangeService } from './exchange.service';
import { ExchangeController } from './exchange.controller';
import { ExchangeDb } from './db.exchange';

@Module({
  controllers: [ExchangeController],
  providers: [ExchangeService,ExchangeDb],
})
export class ExchangeModule {}
