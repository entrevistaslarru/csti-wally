import * as Joi from "joi";

export const ExchangeExchangeValidation=Joi.object({
  value: Joi.number().min(0).required(),
  srcCoin: Joi.string().trim().min(1).required(),
  dstCoint: Joi.string().trim().min(1).required()
})

export const CreateExchangeValidation=Joi.object({
  value: Joi.number().min(0).required(),
  coin: Joi.string().trim().min(1).required(),
})

export const UpdateExchangeValidation=Joi.object({
  body: Joi.object({
    value: Joi.number().min(0).required(),
    coin: Joi.string().trim().min(1).required(),
  }),
  coin:Joi.string().trim().min(1).required(),
})
