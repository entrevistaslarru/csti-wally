import { Test, TestingModule } from '@nestjs/testing';
import { ExchangeController } from './exchange.controller';
import { ExchangeService } from './exchange.service';
import { ExchangeDb } from './db.exchange';
import { ExchangeRequest } from './dto/exchange.dto';

describe('ExchangeController', () => {
  let controller: ExchangeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExchangeController],
      providers: [ExchangeService,ExchangeDb],
    }).compile();

    controller = module.get<ExchangeController>(ExchangeController);
  });
  describe("exchange",()=>{
    it("success",()=>{
      const request:ExchangeRequest={
        value:100,
        srcCoin:"PE",
        dstCoint:"USD"
      }
      const responseApi=controller.exchange(request)
      expect(responseApi.data.value).toEqual(26)
    })

    it("coin NotFound",()=>{
      const request:ExchangeRequest={
        value:100,
        srcCoin:"ECU",
        dstCoint:"USD"
      }
      try{
        controller.exchange(request)
      }catch(e){
        expect(true).toEqual(true)
      }      
    })
  })
  describe("FindAll",()=>{
    it("find",()=>{
      const response=controller.findAll()
      expect(response.data.length).toBeGreaterThan(0)
    })
    it("findAll",()=>{
      const response=controller.findOne("PE")
      expect(response.data.coin).toEqual("PE")
    })
  })

  describe("Update",()=>{
    it("update",()=>{
      const response=controller.update("PE",{coin:"ECU",value:500})
      expect(response.data.coin).toEqual("ECU")
    })
  })

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
